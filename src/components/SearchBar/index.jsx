import React,{useState,useEffect,useRef} from 'react';
//image
import searchIcon from '../../images/search-icon.svg'
//styles
import { Wrapper,Content } from './SearchBar.style';

const SearchBar = ({setSearchTerm}) => {
    const [state,setState] = useState('');
    const initial = useRef(true);

    useEffect(()=>{
        if(initial.current){
            initial.current= false;
            return;
        }
        const timer = setTimeout(()=>{
            setSearchTerm(state)
        },500)


        return()=> clearTimeout(timer)
    },[setSearchTerm,state])
    const searchBarChange = (e)=>{
        setState(e.currentTarget.value)
    }

    return ( 
        <Wrapper>
            <Content>
                <img src={searchIcon} alt='search-icon'/>
                <input 
                    type='text' 
                    placeholder='Search Movie'
                    onChange= {searchBarChange}
                    value= {state}
                />
            </Content>
        </Wrapper>
     );
}
 
export default SearchBar;