import styled from "styled-components";

export  const Wrapper = styled.button`
    display: block;
    background: var(--darkGrey);
    width: 25%;
    min-width: 200px;
    height: 60px;
    border-radius: 30px;
    color: var(--white);
    border: 0;
    font-size: var(--fontBig);
    margin: 20px auto;
    transition: all .5s ease-in-out;
    outline: none;
    :hover{
        opacity: .8
    }
`;
