import React from 'react';
import { Link } from 'react-router-dom';

import RMDBLogo from '../../images/react-movie-logo.svg'
import TMDBLogo from '../../images/tmdb_logo.svg'


import { Wrapper,Content,TMDBLogoImg,LogoImg } from './Header.style';

const Header = () => {
    return ( 
        <Wrapper>
            <Content>
                <Link to='/'>
                    <LogoImg src={RMDBLogo} alt = 'rmd-logo'/>
                </Link>
                <TMDBLogoImg src={TMDBLogo} alt = 'tmd-logo'/>
            </Content>
        </Wrapper>
     );
}
 
export default Header;