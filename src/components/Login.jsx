import React,{useState,useContext} from 'react';
import { useNavigate } from 'react-router';
import API from '../API'

import Button from './Button';

import { Wrapper} from './Login.styled';

import { Context } from '../context.js';

const Login = () => {

    const[username, setUsername] = useState('')
    const[password, setPassword] = useState('')
    const[error,setError] = useState(false) 

    const [user,setUser] = useContext(Context)

    const navigate = useNavigate()

    const handleInput = (e)=>{
        const name = e.currentTarget.name
        const value = e.currentTarget.value

        if(name === 'username') setUsername(value)
        if(name === 'password') setPassword (value)
    }
    const handleSubmit = async ()=>{
        setError(false);
        try{
            const requestToken = await API.getRequestToken()
            const sessionID = await API.authenticate(
                requestToken,
                username,
                password
            )

            setUser({sessionID:sessionID.session_id,username});
            navigate('/');
        }catch{
            setError(true)
        }
    }
    return ( 
        <Wrapper>
            {error && <div className='erroe'>There was an error</div>}
            <label>Username:</label>
            <input 
                type="text" 
                value={username}
                name='username'
                onChange={handleInput}
            />
            <input 
                type="text" 
                value={password}
                name = 'password'
                onChange={handleInput}
            />
            <Button text='login' callback={handleSubmit}/>
        </Wrapper>
     );
}
 
export default Login;