import React from 'react';

import {useHomeFetch} from '../hooks/useHomeFetch'
//API
import API from '../API'
//cofig
import { POSTER_SIZE, BACKDROP_SIZE, IMAGE_BASE_URL } from '../config';
//components
import HeroImage from './HeroImage/index';
import Grid from './Grid/index';
import Thumb  from './Thumb/index';
import Spinner from './spinner/index';
import SearchBar from './SearchBar/index';
import Button from './Button';
//Hook

//Image
import NoImage from '../images/no_image.jpg'

const Home = () => {
    const {state,loading,error,setSearchTerm,searchTerm,setIsLoadingMore} = useHomeFetch();
    if(error){
        return <div>Someting went wrong ...</div>
    }
    return ( 
        <>
            {!searchTerm && state.results[0] ? 
                <HeroImage 
                    image={`${IMAGE_BASE_URL}${BACKDROP_SIZE}${state.results[0].backdrop_path}`}
                    title={state.results[0].original_title}
                    text={state.results[0].overview}    
                />
                : null
            }
            <SearchBar setSearchTerm={setSearchTerm}/>
            <Grid header = {searchTerm? 'Search Result' :'Popular Movies'}>
                {state.results.map(movie=>(
                    <Thumb 
                        key={movie.id}
                        clickable
                        image ={
                            movie.poster_path
                                ? IMAGE_BASE_URL + POSTER_SIZE + movie.poster_path
                                : NoImage
                        }
                        movieId = {movie.id}
                    />
                ))}
            </Grid>
            {loading && <Spinner/>}
            {state.page<state.total_pages && !loading && (
                <Button text='Load More' callback={()=>setIsLoadingMore(true)}/>
            )}
        </>
    );
}
 
export default Home;