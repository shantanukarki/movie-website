import React from 'react';
import { Link } from 'react-router-dom';

import{Image} from './thumb.style'

const Thumb = ({image,movieId,clickable}) => {
    return ( 
        <div>
            {clickable?
                (<Link to={`/${movieId}`}>
                    <Image hover={clickable} src={image} alt='movie-thumb'/>
                </Link>)
                :(
                    <Image hover={clickable} src={image} alt='movie-thumb'/>
                )
            }
        </div>
     );
}
 
export default Thumb;